<h1 align="center">MV-Web Components</h1> <br>
<!--<p align="center">
    <img alt="mv-wc logo" title="mv-wc logo" src="#" width="300">
</p>-->
<div align="center">
  <strong>Web components library</strong>
</div>
<div align="center">
  Web components for the Valais Media Library for the library applications.
</div>

<div align="center">
  <h3>
    <a href="https://valais-media-library.gitlab.io/mv-web-components/">Access</a>
    <span> | </span>
    <a href="#">Documentation</a>
    <span> | </span>
    <a href="#contributing">
      Contributing
    </a>
  </h3>
</div>

<div align="center">
  <sub>Built with ❤︎ in Valais by <a href="https://gitlab.com/valais-media-library/mv-web-components/-/graphs/master">contributors</a>
</div>


# Introduction

[![license](https://img.shields.io/badge/license-MIT-green.svg?style=flat-square)](https://gitlab.com/valais-media-library/mv-web-components/blob/master/LICENSE)
[![pipeline status](https://gitlab.com/valais-media-library/mv-web-components/badges/master/pipeline.svg)](https://gitlab.com/valais-media-library/mv-web-components/commits/master)
![Built With Stencil](https://img.shields.io/badge/-Built%20With%20Stencil-16161d.svg?logo=data%3Aimage%2Fsvg%2Bxml%3Bbase64%2CPD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjIuMSwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IgoJIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTIgNTEyOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI%2BCjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI%2BCgkuc3Qwe2ZpbGw6I0ZGRkZGRjt9Cjwvc3R5bGU%2BCjxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik00MjQuNywzNzMuOWMwLDM3LjYtNTUuMSw2OC42LTkyLjcsNjguNkgxODAuNGMtMzcuOSwwLTkyLjctMzAuNy05Mi43LTY4LjZ2LTMuNmgzMzYuOVYzNzMuOXoiLz4KPHBhdGggY2xhc3M9InN0MCIgZD0iTTQyNC43LDI5Mi4xSDE4MC40Yy0zNy42LDAtOTIuNy0zMS05Mi43LTY4LjZ2LTMuNkgzMzJjMzcuNiwwLDkyLjcsMzEsOTIuNyw2OC42VjI5Mi4xeiIvPgo8cGF0aCBjbGFzcz0ic3QwIiBkPSJNNDI0LjcsMTQxLjdIODcuN3YtMy42YzAtMzcuNiw1NC44LTY4LjYsOTIuNy02OC42SDMzMmMzNy45LDAsOTIuNywzMC43LDkyLjcsNjguNlYxNDEuN3oiLz4KPC9zdmc%2BCg%3D%3D&colorA=16161d&style=flat-square)


Web components for the Valais Media Library for the library applications.

## Usage

Use one of this solution to "install" the `mv-web-components` in your project.

### Script tag

- Put a script tag similar to this `<script src='https://unpkg.com/@valais-media-library/mv-web-components@latest/dist/mv-web-components.js'></script>` in the head of your index.html
- Then you can use the element anywhere in your template, JSX, html, etc.

### Node Modules
- Run `npm install @valais-media-library/mv-web-components --save`
- Put a script tag similar to this `<script src='node_modules/@valais-media-library/mv-web-components/dist/mv-web-components.js'></script>` in the head of your index.html
- Then you can use the element anywhere in your template, JSX, html, etc.

### In a stencil-starter app
- Run `npm install @valais-media-library/mv-web-components --save`
- Add an import to the npm packages `@valais-media-library/mv-web-components;`
- Then you can use the element anywhere in your template, JSX, html, etc.


## Documentation

Check the documentation for more informations : Comming soon…

## To start
- **serve** the project : `$ npm run start`
- **build** the project : `$ npm run build`
- **test** the project : `$ npm run test`
- **publish** the build : `$ npm run publish`

Need help? Check out the stencil.js documentation [here](https://stenciljs.com/docs/my-first-component).

## Authors and acknowledgment

* **Generator**: [Stencil](https://github.com/ionic-team/stencil) is a compiler for building fast web apps using Web Components. Stencil combines the best concepts of the most popular frontend frameworks into a compile-time rather than run-time tool.  Stencil takes TypeScript, JSX, a tiny virtual DOM layer, efficient one-way data binding, an asynchronous rendering pipeline (similar to React Fiber), and lazy-loading out of the box, and generates 100% standards-based Web Components that run in any browser supporting the Custom Elements v1 spec.

* **Michael Ravedoni** - *Initial work* - [michaelravedoni](https://gitlab.com/michaelravedoni)

See also the list of [contributors](https://gitlab.com/valais-media-library/mv-web-components/-/graphs/master) who participated in this project.

## License

[MIT License](https://opensource.org/licenses/MIT)
