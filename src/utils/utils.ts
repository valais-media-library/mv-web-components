
function browserLang(): string {
  var availableLanguages = ["fr", "de"];

  var locale =
  [
    ...(window.navigator.languages || []),
    window.navigator.language
  ]
  .filter(Boolean)
  .map(language => language.substr(0, 2))
  .find(language => availableLanguages.includes(language)) || "fr";
  return (locale);
}

function urlParamLang(): string {
  const urlParams = new URLSearchParams(window.location.search);
  var langParam = urlParams.has('lang') ? urlParams.get('lang') : null;
  return (langParam);
}

export function userLang(): string {
  var userLang = urlParamLang() == null ? browserLang() : urlParamLang();
  return (userLang);
}
