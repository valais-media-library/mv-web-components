# mv-nav-inter



<!-- Auto Generated Below -->


## Properties

| Property       | Attribute      | Description        | Type     | Default                                                                                                 |
| -------------- | -------------- | ------------------ | -------- | ------------------------------------------------------------------------------------------------------- |
| `applications` | `applications` | Applications array | `string` | `'[{"name": "MV-Classification", "url": "https://valais-media-library.gitlab.io/mv-classification/"}]'` |
| `dasboardUrl`  | `dasboard-url` | Dashboard url      | `string` | `'https://valais-media-library.gitlab.io/mv-dashboard/'`                                                |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
