import { Component, Prop, h } from '@stencil/core';

@Component({
  tag: 'mv-nav-inter',
  styleUrl: 'mv-nav-inter.css',
  shadow: false
})
export class MvNavInter {
  /**
   * Applications array
   */
  @Prop() applications: string = '[{"name": "MV-Classification", "url": "https://valais-media-library.gitlab.io/mv-classification/"}]';

  /**
   * Dashboard url
   */
  @Prop() dasboardUrl: string = 'https://valais-media-library.gitlab.io/mv-dashboard/';

  render() {
    return (
      <div class="mv-tile bg-light mv-tile-xsmall text-small d-none d-lg-block">
        <div class="container">
          <div class="row">
            <div class="col">
            {JSON.parse(this.applications).map((a, i) =>
              <span><a href={a.url}>{a.name}</a> { i != JSON.parse(this.applications).length - 1 ? ' | ' : '' }</span>
            )}
            </div>
          <div class="col-auto">
            <a href={this.dasboardUrl}>dashboard &rarr;</a>
          </div>
        </div>
      </div>
    </div>
    );
  }
}
