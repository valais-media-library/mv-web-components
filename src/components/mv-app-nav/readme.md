# mv-app-nav



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description          | Type     | Default                                                                                                                                            |
| ------------- | -------------- | -------------------- | -------- | -------------------------------------------------------------------------------------------------------------------------------------------------- |
| `addUrl`      | `add-url`      | Add url              | `string` | `undefined`                                                                                                                                        |
| `appName`     | `app-name`     | Application name     | `string` | `'MV-App'`                                                                                                                                         |
| `appRootUrl`  | `app-root-url` | Application root url | `string` | `'./'`                                                                                                                                             |
| `helpUrl`     | `help-url`     | Help url             | `string` | `undefined`                                                                                                                                        |
| `loginUrl`    | `login-url`    | Login url            | `string` | `undefined`                                                                                                                                        |
| `logoutUrl`   | `logout-url`   | Logout url           | `string` | `undefined`                                                                                                                                        |
| `nav`         | `nav`          | Nav array            | `string` | `'[{"name": {"fr": "Accueil", "de": "Home", "en": "Home"}, "url": "./"}, {"name": {"fr": "Aide", "de": "Hilfe", "en": "Help"}, "url": "./help"}]'` |
| `settingsUrl` | `settings-url` | Settings url         | `string` | `undefined`                                                                                                                                        |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
