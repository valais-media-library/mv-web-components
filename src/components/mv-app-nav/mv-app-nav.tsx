import { Component, Prop, h } from '@stencil/core';
import { userLang } from '../../utils/utils';

@Component({
  tag: 'mv-app-nav',
  styleUrl: 'mv-app-nav.css',
  shadow: false
})
export class MvAppNav {
  /**
   * Application name
   */
  @Prop() appName: string = 'MV-App';

  /**
   * Application root url
   */
  @Prop() appRootUrl: string = './';

  /**
   * Nav array
   */
  @Prop() nav: string = '[{"name": {"fr": "Accueil", "de": "Home", "en": "Home"}, "url": "./"}, {"name": {"fr": "Aide", "de": "Hilfe", "en": "Help"}, "url": "./help"}]';

  /**
   * Add url
   */
  @Prop() addUrl: string;

  /**
   * Help url
   */
  @Prop() helpUrl: string;

  /**
   * Login url
   */
  @Prop() loginUrl: string;

  /**
   * Logout url
   */
  @Prop() logoutUrl: string;

  /**
   * Settings url
   */
  @Prop() settingsUrl: string;

  /**
   * The component will load but has not rendered yet.
   *
   * This is a good place to make any last minute updates before rendering.
   *
   * Will only be called once
   */
  componentWillLoad() {
    userLang();
  }

  private messages(): Object {
    var messages = {
      "fr": {
        "menu": "Menu",
        "help": "Aide",
        "login": "S'identifier",
        "logout": "Se déconnecter",
        "settings": "Paramètres",
        "add": "Ajouter",
      },
      "de": {
        "menu": "Menu",
        "help": "Hilfe",
        "login": "Einloggen",
        "logout": "ausloggen",
        "settings": "Einstellungen",
        "add": "zusetzen",
      },
      "en": {
        "menu": "Menu",
        "help": "Help",
        "login": "Login",
        "logout": "Logout",
        "settings": "Settings",
        "add": "Add",
      }
    };
    return messages;
  }

  private i18n(msg, fallback): string {
    if (this.messages()[userLang()] != undefined) {
      var string = this.messages()[userLang()][msg];
    } else {
      var string = fallback;
    }
    return string;
  }

  addParam(event: UIEvent) {
    let e: any = event;
    let d: any = e.target.dataset;
    let param = d.param;
    let value = d.paramValue;
    let w: any = window;
    const url: any = new URL(w.location);
    url.searchParams.set(param, value);
    location.href = url;
  }

  render() {
    return (
      <nav class="navbar navbar-expand-lg bg-tertiary mv-navbar-app">
        <div class="container">
          <a class="navbar-brand" href={this.appRootUrl}>
            <img src="https://unpkg.com/mv-styleguide@1.0.0/images/logo/logo-mv-square.svg" width="30" height="30" class="d-inline-block align-top mr-2" alt={this.appName + 'logo'} />
            {this.appName}
          </a>
          <div class="btn btn-outline-white nav-toggle-mobile d-lg-none">
            {this.i18n('menu', 'Menu')}
              <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
              </div>
          </div>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-6 mr-auto">
              {JSON.parse(this.nav).map((n) =>
                <li class="nav-item"><a class="nav-link" href={n.url}>{n.name[userLang()]}</a></li>
              )}
            </ul>
            <div class="btn-group btn-group-sm" role="group" aria-label="Navigation shortcut">
              { this.addUrl ? <a href={this.addUrl} class="btn p-1"><span class="fas fa-plus" title={this.i18n('add', 'Add')}></span></a> : ''}
              { this.helpUrl ? <a href={this.helpUrl} class="btn p-1"><span class="fas fa-question-circle" title={this.i18n('help', 'Help')}></span></a> : ''}
              { this.logoutUrl ? <a href={this.logoutUrl} class="btn p-1"><span class="fas fa-sign-out-alt" title={this.i18n('logout', 'Logout')}></span></a> : ''}
              { this.loginUrl ? <a href={this.loginUrl} class="btn p-1"><span class="fas fa-sign-in-alt" title={this.i18n('login', 'Login')}></span></a> : ''}
              { this.settingsUrl ? <a href={this.settingsUrl} class="btn p-1"><span class="fas fa-sliders-h" title={this.i18n('settings', 'Settings')}></span></a> : ''}
            </div>
            <nav class="nav-lang nav-lang-short ml-4 mr-0">
              <ul>
                <li><a aria-label="Français" class={userLang() == 'fr' ? 'active' : ''} onClick={this.addParam.bind(this)} data-param="lang" data-param-value="fr">FR</a></li>
                <li><a aria-label="Deutsch" class={userLang() == 'de' ? 'active' : ''} onClick={this.addParam.bind(this)} data-param="lang" data-param-value="de">DE</a></li>
                <li><a aria-label="English" class={userLang() == 'en' ? 'active' : ''} onClick={this.addParam.bind(this)} data-param="lang" data-param-value="en">EN</a></li>
              </ul>
            </nav>
          </div>
        </div>
      </nav>
    );
  }
}
