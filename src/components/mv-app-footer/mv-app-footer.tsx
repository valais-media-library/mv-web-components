import { Component, Prop, h } from '@stencil/core';
import { userLang } from '../../utils/utils';

@Component({
  tag: 'mv-app-footer',
  styleUrl: 'mv-app-footer.css',
  shadow: false
})
export class MvAppFooter {
  /**
   * Impressum URL
   */
  @Prop() impressumUrl: string = './impressum';

  /**
   * Impressum label
   */
  @Prop() impressumLabel: string = 'Impressum';

  /**
   * Help URL
   */
  @Prop() helpUrl: string = './help';

  /**
   * Help label
   */
  @Prop() helpLabel: string = 'Aide';

  /**
   * E-mail
   */
  @Prop() email: string = 'mv-webmaster@admin.vs.ch';

  /**
   * Text
   */
  @Prop() text: string = '';

  /**
   * Error label
   */
  @Prop() errorLabel: string = 'Signaler une erreur';

  /**
   * Error email body
   */
  @Prop() errorEmailBody: string = '';

  /**
   * Suggest label
   */
  @Prop() suggestLabel: string = 'Suggérer une amélioration';

  /**
   * Suggest email body
   */
  @Prop() suggestEmailBody: string = '';

  /**
   * Custom class
   */
  @Prop() customClass: string = '';

  /**
   * Suggest email body
   */
  @Prop() repository: string = '';

  /**
   * The component will load but has not rendered yet.
   *
   * This is a good place to make any last minute updates before rendering.
   *
   * Will only be called once
   */
  componentWillLoad() {
    userLang();
  }

  private suggestMailto(): string {
    return 'mailto:' + this.email + '?Subject='+ encodeURIComponent(this.suggestLabel) +'&amp;Body='+ encodeURIComponent(this.suggestEmailBody);
  }

  private errorMailto(): string {
    return 'mailto:' + this.email + '?Subject='+ encodeURIComponent(this.errorLabel) +'&amp;Body='+ encodeURIComponent(this.errorEmailBody);
  }

  private messages(): Object {
    var messages = {
      "fr": {
        "impressum": "Impressum",
        "help": "Aide",
        "suggest": "Suggérer une amélioration",
        "error": "Signaler une erreur",
      },
      "de": {
        "impressum": "Impressum",
        "help": "Hilfe",
        "suggest": "Verbesserung vorschlagen",
        "error": "Fehler melden",
      },
      "en": {
        "impressum": "Impressum",
        "help": "Help",
        "suggest": "Suggest an improvement",
        "error": "Report an error",
      }
    };
    return messages;
  }

  private i18n(msg, fallback): string {
    if (this.messages()[userLang()] != undefined) {
      var string = this.messages()[userLang()][msg];
    } else {
      var string = fallback;
    }
    return string;
  }

  render() {
    return (
      <footer class={'footer '+ this.customClass} role="contentinfo">
        <div class="footer-legal">
          <div class="footer-legal-links">
            <a href={this.impressumUrl}>{this.i18n('impressum', this.impressumLabel)}</a>
            <a href={this.helpUrl}>{this.i18n('help', this.helpLabel)}</a>
          </div>
          <div>
            <p>Made with <span class="hearth">&hearts;</span> in Valais.{this.text ? ' ' + this.text : ''}</p>
            <p><slot /></p>
          </div>
          <div class="footer-legal-links">
            <a href={this.suggestMailto()}>{this.i18n('suggest', this.suggestLabel)}</a> {this.repository ? <a href={this.repository} class="ml-2"><span class="fab fa-gitlab" title="Source code"></span></a> : '' }
            <a href={this.errorMailto()}>{this.i18n('error', this.errorLabel)} →</a>
          </div>
        </div>
      </footer>
    );
  }
}
