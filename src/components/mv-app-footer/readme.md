# mv-app-footer



<!-- Auto Generated Below -->


## Properties

| Property           | Attribute            | Description        | Type     | Default                       |
| ------------------ | -------------------- | ------------------ | -------- | ----------------------------- |
| `customClass`      | `custom-class`       | Custom class       | `string` | `''`                          |
| `email`            | `email`              | E-mail             | `string` | `'mv-webmaster@admin.vs.ch'`  |
| `errorEmailBody`   | `error-email-body`   | Error email body   | `string` | `''`                          |
| `errorLabel`       | `error-label`        | Error label        | `string` | `'Signaler une erreur'`       |
| `helpLabel`        | `help-label`         | Help label         | `string` | `'Aide'`                      |
| `helpUrl`          | `help-url`           | Help URL           | `string` | `'./help'`                    |
| `impressumLabel`   | `impressum-label`    | Impressum label    | `string` | `'Impressum'`                 |
| `impressumUrl`     | `impressum-url`      | Impressum URL      | `string` | `'./impressum'`               |
| `repository`       | `repository`         | Suggest email body | `string` | `''`                          |
| `suggestEmailBody` | `suggest-email-body` | Suggest email body | `string` | `''`                          |
| `suggestLabel`     | `suggest-label`      | Suggest label      | `string` | `'Suggérer une amélioration'` |
| `text`             | `text`               | Text               | `string` | `''`                          |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
