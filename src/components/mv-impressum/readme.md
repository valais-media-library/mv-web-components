# mv-impressum



<!-- Auto Generated Below -->


## Properties

| Property           | Attribute           | Description       | Type      | Default                                            |
| ------------------ | ------------------- | ----------------- | --------- | -------------------------------------------------- |
| `analytics`        | `analytics`         | Analytics         | `boolean` | `true`                                             |
| `customClass`      | `custom-class`      | Custom class      | `string`  | `undefined`                                        |
| `generatorLabel`   | `generator-label`   | Generator label   | `string`  | `'Gitlab Pages'`                                   |
| `generatorUrl`     | `generator-url`     | Generator url     | `string`  | `'https://docs.gitlab.com/ee/user/project/pages/'` |
| `newsletter`       | `newsletter`        | Newsletter        | `boolean` | `true`                                             |
| `organizationName` | `organization-name` | Organization name | `string`  | `'Médiathèque Valais'`                             |
| `responsibleEmail` | `responsible-email` | Responsible email | `string`  | `'mv-direction@admin.vs.ch'`                       |
| `siteName`         | `site-name`         | Site name         | `string`  | `'mediatheque.ch'`                                 |
| `updatedDate`      | `updated-date`      | Updtate date      | `string`  | `'30 juillet 2019'`                                |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
