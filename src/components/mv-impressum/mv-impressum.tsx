import { Component, Prop, h } from '@stencil/core';
import { userLang } from '../../utils/utils';

@Component({
  tag: 'mv-impressum',
  styleUrl: 'mv-impressum.css',
  shadow: false
})
export class MvImpressum {
  /**
   * Custom class
   */
  @Prop() customClass: string;

  /**
   * Organization name
   */
  @Prop() organizationName: string = 'Médiathèque Valais';

  /**
   * Site name
   */
  @Prop() siteName: string = 'mediatheque.ch';

  /**
   * Responsible email
   */
  @Prop() responsibleEmail: string = 'mv-direction@admin.vs.ch';

  /**
   * Generator label
   */
  @Prop() generatorLabel: string = 'Gitlab Pages';

  /**
   * Generator url
   */
  @Prop() generatorUrl: string = 'https://docs.gitlab.com/ee/user/project/pages/';

  /**
   * Analytics
   */
  @Prop() analytics: boolean = true;

  /**
   * Newsletter
   */
  @Prop() newsletter: boolean = true;

  /**
   * Updtate date
   */
  @Prop() updatedDate: string = '30 juillet 2019';

  /**
   * The component will load but has not rendered yet.
   *
   * This is a good place to make any last minute updates before rendering.
   *
   * Will only be called once
   */
  componentWillLoad() {
    userLang();

    if(navigator.doNotTrack == '1') {
      document.getElementById("dnt").innerHTML = "<em>Vous n'êtes pas suivi</em> parce que votre navigateur nous indique que vous ne voulez pas l'être.";
    }
    else {
      document.getElementById("dnt").innerHTML = '<em>Vous êtes suivi</em>. Suivez ces <a href="https://allaboutdnt.com/#adjust-settings" target="_blank">instructions</a> si vous ne souhaitez pas l\'être.';
    }
  }

  render() {
    return (
      <section class={this.customClass}>
        <h2>Impressum</h2>
        <h3>Responsabilité</h3>
        <dl>
          <dt>{this.organizationName}</dt>
          <dd><a href={'mailto:' + this.responsibleEmail}>{this.responsibleEmail}</a></dd>
        </dl>

        <h3>Technique et graphisme</h3>
        <dl class="at-description-list-horizontal">
        <dt>Générateur</dt>
        <dd><a href={this.generatorUrl}>{this.generatorLabel}</a></dd>
        <dt>Services utilisés</dt>
        { this.analytics ? (<dd><a href="https://Matomo.org">Matomo Analytics</a></dd>) : ''}
        <dt>Graphisme</dt>
        <dd><a href="https://gitlab.com/valais-media-library/mv-styleguide">MV-Styleguide</a></dd>
        </dl>

        <h2 id="cgu">Conditions générales d’utilisation</h2>
        <p>Sauf mention contraire, toutes les informations ou documents diffusés sur { this.siteName } sont mises à disposition par la {this.organizationName} selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>. Cependant, cette licence d’utilisation ne s’applique pas aux informations et liens externes vers lesquels ce site renvoie. De plus, le téléchargement ou la copie de textes, d'illustrations, de photographies ou d'autres données n'entraîne aucun transfert de droits sur les contenus. Les autorisations au-delà du champ de cette licence peuvent être obtenues ici même.</p>
        <p class="text-center"><a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licence Creative Commons" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a></p>
        <p>Vous êtes autorisé à :</p>
        <p><strong>Partager</strong> — copier, distribuer et communiquer le matériel par tous moyens et sous tous formats</p>
        <p><strong>Adapter</strong> — remixer, transformer et créer à partir du matériel pour toute utilisation, y compris commerciale.</p>
        <p>Selon les conditions suivantes :</p>
        <p><strong>Attribution</strong> — Vous devez créditer l'Œuvre, intégrer un lien vers la licence et indiquer si des modifications ont été effectuées à l'Oeuvre. Vous devez indiquer ces informations par tous les moyens raisonnables, sans toutefois suggérer que l'Offrant vous soutient ou soutient la façon dont vous avez utilisé son Oeuvre.</p>
        <p><strong>Partage dans les Mêmes Conditions</strong> — Dans le cas où vous effectuez un remix, que vous transformez, ou créez à partir du matériel composant l'Oeuvre originale, vous devez diffuser l'Oeuvre modifiée dans les même conditions, c'est à dire avec la même licence avec laquelle l'Oeuvre originale a été diffusée.</p>
        <p>Pas de restrictions complémentaires — Vous n'êtes pas autorisé à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser l'Oeuvre dans les conditions décrites par la licence.</p>

        <h2>Informations légales</h2>
        <h3>Champ d’application</h3>
        <p>Les présentes informations et règles d'utilisation se rapportent exclusivement au site Internet { this.siteName } ainsi qu'aux autres sites renvoyant à ce dernier (constituant ensemble le « portail »). Ils sont gérés par la personne morale ou physique mentionnée dans l'impressum du portail (ci-après l'« exploitant »). Les dispositions spéciales énoncées dans le portail ainsi que dans les autres sites Internet de sujets de droit { this.siteName } demeurent expressément réservées.</p>
        <p>Les informations et règles relatives à l’utilisation du présent portail peuvent être modifiées en tout temps avec effet de droit.</p>
        <h3>Limitation de la responsabilité</h3>
        <p>Malgré la grande attention qu’il portent à la justesse des informations diffusées sur ce site, { this.siteName } ne peut endosser aucune responsabilité quant à la fidélité, à l’exactitude, à l’actualité, à la fiabilité et à l’intégralité de ces informations.</p>
        <p>{ this.siteName } se réserve expressément le droit de modifier en partie ou en totalité le contenu de ce site, de le supprimer ou d’en suspendre temporairement la diffusion, et ce à tout moment et sans avertissement préalable.</p>
        <p>{ this.siteName } ne saurait être tenues pour responsables des dommages matériels ou immatériels qui pourraient être causés par l’accès aux informations diffusées ou par leur utilisation ou non-utilisation, par le mauvais usage de la connexion ou par des problèmes techniques.</p>
        <h3>Renvois et liens vers d’autres sites internet</h3>
        <p>Les renvois et liens vers d’autres sites Internet ne sont pas de la responsabilité de { this.siteName }. L’accès à ces sites et leur utilisation se font aux risques des utilisateurs. { this.siteName } déclare expressément qu’il n’a aucune influence sur la forme, le contenu et les offres des sites auxquels il renvoit. Les informations et services offerts par ces sites sont entièrement de la responsabilité de leurs auteurs.</p>
        <p>{ this.siteName } rejettent toute responsabilité pour de tels sites Internet.</p>
        <h3>Protection des données</h3>
        <p>L’Art. 13 <em><a href="https://www.admin.ch/opc/fr/classified-compilation/19995395/index.html#a13">Protection de la sphère privée</a></em> de la <em>Constitution fédérale de la Confédération suisse</em>, la <a href="https://www.admin.ch/opc/fr/classified-compilation/19920153/index.html" target="_blank"><em>Loi fédérale sur la protection des données</em></a> (LPD) et son <a href="https://www.admin.ch/opc/fr/classified-compilation/19930159/index.html" target="_blank">Ordonnance relative</a> (OLPD) disposent que toute personne a droit à la protection de sa sphère privée, ainsi qu’à la protection contre l’emploi abusif des données personnelles et sensibles qui la concernent. { this.siteName } observe strictement ces dispositions.</p>
        <p>Les données personnelles sont traitées de façon confidentielle et ne sont ni vendues, ni transmises à des tiers sans l’accord préalable de la personne concernée.</p>
        <p>Aucune donnée personnelle n’est enregistrée lors de l’accès à ce site. Le serveur internet n’enregistre que des données statistiques d’utilisation non personnelle, par des cookies, pour analyser l’appréciation de la qualité de son offre (fichiers, journaux).</p>
        { this.analytics ? (
        <p>Ce site utilise <a href="https://matomo.org/" target="_blank">Matomo</a> pour analyser l’audience du site et améliorer son contenu notamment pour établir le nombre quotidien de visites du site. Les données d’utilisation ne sont donc pas associées à des données personnelles. Vous trouverez plus d’informations sur les règles de confidentialité de cet outil sur le <a href="https://matomo.org/privacy/">site de Matomo</a> ou dans la section <em><a href="#matomo">Information sur Matomo</a></em>.</p>
        ) : ''}
        <p>Ces règles ne sont toutefois pas valables pour la prise de contact volontaire. Lors de l’enregistrement de votre adresse électronique, celle-ci est stockée dans une banque de données séparée, qui ne possède aucun lien avec les fichiers journaux anonymes. Vous avez à tout moment la possibilité d’annuler votre enregistrement.</p>
        <p>Les adresses e-mails ou autres données permettant d’identifier la personne ne sont en aucun cas transmises à des entités externes à { this.siteName } sans l’accord préalable des personnes concernées. Seul le webmaster du site conserve les e-mails reçus par le site.</p>
        <h3>Transfert de données via Internet</h3>
        <p>En tant que réseau ouvert accessible à quiconque, Internet constitue fondamentalement un environnement non sécurisé. Quand bien même la transmission des paquets individuels de données a lieu en principe sous une forme cryptée, cette procédure ne s’applique ni à l’expéditeur ni au destinataire. En outre, même si l’expéditeur et le destinataire se trouvent en Suisse, il peut arriver que les données transitent vers des pays étrangers connaissant un niveau de protection des données inférieur à celui de la Suisse. L'exploitant décline toute responsabilité quant à la sécurité des données durant leur transfert via Internet.</p>
        <h3>Droit applicable / for juridique</h3>
        <p>Dans les limites de la loi, tous les rapports juridiques entre les utilisateurs et les utilisateurs des sites internet et { this.siteName } sont soumis au droit matériel suisse. Seul le droit suisse est applicable.</p>
        <h3>Licence</h3>
        <p>Voir les <a href="#cgu">Conditions générales d’utilisation</a>.</p>
        <h2>Services utilisés</h2>
        { this.newsletter ? (
        <h3>Newsletter</h3>
        ) : ''}
        { this.newsletter ? (
        <p>La newsletter n'est envoyée aux clients que s’ils en font la demande expresse. L’abonnement à la newsletter peut être résilié en tout temps. Des précisions à cet égard figurent sur la newsletter.</p>
        ) : ''}
        { this.newsletter ? (
        <p>Pour l’envoi de sa newsletter électronique, { this.siteName } utilise un système permettant une évaluation anonyme de l’envoi. Aucune information personnelle n’est récoltée ni utilisée sans l’autorisation explicite des personnes concernées. Toute personne abonnée à la newsletter électronique de { this.siteName } peut s’en désabonner facilement et en tout temps. Chaque newsletter contient un lien prévu à cet effet.</p>
        ) : ''}

        { this.analytics ? (
        <h3 id="matomo">Matomo</h3>
        ) : ''}
        { this.analytics ? (
        <div>
        <p>Lorsqu’un visiteur se connecte au site { this.siteName }, seules des données impersonnelles, délivrant des informations sur les liens les plus utilisés et les pages les plus visitées, en fonction de l’heure et de la durée de la visite, sont enregistrées. Les visiteurs du site { this.siteName } restent par conséquent totalement anonymes.</p>
        <p>Ce site utilise <em><a href="https://matomo.org" target="_blank">Matomo</a></em> comme outil d’analyse de l’audience du site. <em>Piwki</em> est un logiciel libre et open source de mesure de statistiques web. Il utilise les cookies pour en savoir plus sur votre utilisation du site Web. Il a été configuré de telle sorte à anonymiser votre adresse IP. Les données récoltées sont enregistrées dans une banque de données MySQL privée, hébergée avec le site web et ne sont jamais transmises à des tiers.</p>
        <p>Grâce à l'entête HTTP <em>do not track</em> (DNT), en cours de standardisation par le W3C, vous pouvez désactiver votre suivi par <em>Matomo</em>. Pour désactiver le suivi, merci de suivre les isntructions présentes dans lien ci-dessous <small>(s’il n’y en a pas, c’est que vous n’êtes actuellement pas suivi)</small>&nbsp;:</p>

        <div class="mv-tile bg-light p-2 mb-2">
		      <p><span id="dnt"><em>Vous êtes suivi</em>. Suivez ces <a href="https://allaboutdnt.com/#adjust-settings">instructions</a> si vous ne souhaitez pas l'être.</span></p>
	      </div>
        <p>Vous trouverez davantage d’informations sur Matomo et sur la protection des données sous : <a href="https://matomo.org/privacy" target="_blank">https://matomo.org/privacy</a>.</p>
        </div>) : ''}
        <p>Dernière mise à jour : { this.updatedDate }</p>
      <slot />
    </section>
    );
  }
}
